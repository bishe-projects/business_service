package main

import (
	"github.com/cloudwego/kitex/server"
	business "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/business/businessservice"
	"log"
	"net"
)

func main() {
	addr, _ := net.ResolveTCPAddr("tcp", "127.0.0.1:8882")
	svr := business.NewServer(new(BusinessServiceImpl), server.WithServiceAddr(addr))

	err := svr.Run()

	if err != nil {
		log.Println(err.Error())
	}
}
