package main

import (
	"context"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/business_service/src/interface/facade"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/business"
)

// BusinessServiceImpl implements the last service interface defined in the IDL.
type BusinessServiceImpl struct{}

// CreateBusiness implements the BusinessServiceImpl interface.
func (s *BusinessServiceImpl) CreateBusiness(ctx context.Context, req *business.CreateBusinessReq) (resp *business.CreateBusinessResp, err error) {
	klog.CtxInfof(ctx, "CreateBusiness req=%+v", req)
	resp = facade.BusinessFacade.CreateBusiness(ctx, req)
	klog.CtxInfof(ctx, "CreateBusiness resp=%+v", resp)
	return
}

// GetBusinessByID implements the BusinessServiceImpl interface.
func (s *BusinessServiceImpl) GetBusinessByID(ctx context.Context, req *business.GetBusinessByIDReq) (resp *business.GetBusinessByIDResp, err error) {
	klog.CtxInfof(ctx, "GetBusinessByID req=%+v", req)
	resp = facade.BusinessFacade.GetBusinessByID(ctx, req)
	klog.CtxInfof(ctx, "GetBusinessByID resp=%+v", resp)
	return
}

// GetBusinessByName implements the BusinessServiceImpl interface.
func (s *BusinessServiceImpl) GetBusinessByName(ctx context.Context, req *business.GetBusinessByNameReq) (resp *business.GetBusinessByNameResp, err error) {
	klog.CtxInfof(ctx, "GetBusinessByName req=%+v", req)
	resp = facade.BusinessFacade.GetBusinessByName(ctx, req)
	klog.CtxInfof(ctx, "GetBusinessByName resp=%+v", resp)
	return
}

// AllBusinessList implements the BusinessServiceImpl interface.
func (s *BusinessServiceImpl) AllBusinessList(ctx context.Context, req *business.AllBusinessListReq) (resp *business.AllBusinessListResp, err error) {
	klog.CtxInfof(ctx, "AllBusinessList req=%+v", req)
	resp = facade.BusinessFacade.AllBusinessList(ctx, req)
	klog.CtxInfof(ctx, "AllBusinessList resp=%+v", resp)
	return
}
