package service

import (
	"gitlab.com/bishe-projects/business_service/src/domain/business/entity"
	"gitlab.com/bishe-projects/common_utils/business_error"
)

var BusinessDomain = new(Business)

type Business struct{}

func (d *Business) CreateBusiness(business *entity.Business) *business_error.BusinessError {
	return business.CreateBusiness()
}

func (d *Business) GetBusinessByID(businessID int64) (*entity.Business, *business_error.BusinessError) {
	business := new(entity.Business)
	err := business.GetBusinessByID(businessID)
	if err == business_error.DataNotFoundErr {
		return nil, nil
	}
	return business, err
}

func (d *Business) GetBusinessByName(businessName string) (*entity.Business, *business_error.BusinessError) {
	business := new(entity.Business)
	err := business.GetBusinessByName(businessName)
	if err == business_error.DataNotFoundErr {
		return nil, nil
	}
	return business, err
}

func (d *Business) AllBusinessList() ([]*entity.Business, *business_error.BusinessError) {
	businessList := new(entity.BusinessList)
	err := businessList.AllBusinessList()
	return *businessList, err
}
