package entity

import (
	"fmt"
	"github.com/cloudwego/kitex/pkg/klog"
	biz_error "gitlab.com/bishe-projects/business_service/src/infrastructure/business_error"
	"gitlab.com/bishe-projects/business_service/src/infrastructure/repo"
	"gitlab.com/bishe-projects/business_service/src/infrastructure/repo/po"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/common_utils/error_enum"
	"gorm.io/gorm"
)

type Business struct {
	ID   int64
	Name string
	Desc string
}

func (b *Business) CreateBusiness() *business_error.BusinessError {
	businessPO := b.ConvertToBusinessPO()
	err := repo.BusinessMySQLRepo.BusinessDao.CreateBusiness(businessPO)
	if err == error_enum.ErrDuplicateEntry {
		return business_error.DataExistsErr
	}
	if err != nil {
		klog.Errorf("[BusinessAggregate] create business failed: err=%s", err)
		return biz_error.CreateBusinessErr
	}
	b.fillBusinessFromBusinessPO(businessPO)
	return nil
}

func (b *Business) GetBusinessByID(businessID int64) *business_error.BusinessError {
	businessPO, err := repo.BusinessMySQLRepo.BusinessDao.GetBusinessByID(businessID)
	if err == gorm.ErrRecordNotFound {
		return business_error.DataNotFoundErr
	}
	if err != nil {
		klog.Errorf("[BusinessAggregate] get business by id failed: err=%s", err)
		return biz_error.GetBusinessByIDErr
	}
	b.fillBusinessFromBusinessPO(businessPO)
	return nil
}

func (b *Business) GetBusinessByName(businessName string) *business_error.BusinessError {
	businessPO, err := repo.BusinessMySQLRepo.BusinessDao.GetBusinessByName(businessName)
	if err == gorm.ErrRecordNotFound {
		return business_error.DataNotFoundErr
	}
	if err != nil {
		klog.Errorf("[BusinessAggregate] get business by name failed: err=%s", err)
		return biz_error.GetBusinessByNameErr
	}
	b.fillBusinessFromBusinessPO(businessPO)
	return nil
}

type BusinessList []*Business

func (bl *BusinessList) AllBusinessList() *business_error.BusinessError {
	businessPOList, err := repo.BusinessMySQLRepo.BusinessDao.AllBusinessList()
	if err != nil {
		klog.Errorf("[BusinessAggregate] get all business list failed: err=%s", err)
		return biz_error.GetAllBusinessListErr
	}
	bl.fillBusinessListFromBusinessPOList(businessPOList)
	return nil
}

// converter
func (b *Business) ConvertToBusinessPO() *po.Business {
	return &po.Business{
		ID:   b.ID,
		Name: b.Name,
		Desc: b.Desc,
	}
}

func (b *Business) fillBusinessFromBusinessPO(businessPO *po.Business) {
	b.ID = businessPO.ID
	b.Name = businessPO.Name
	b.Desc = businessPO.Desc
}

func (bl *BusinessList) fillBusinessListFromBusinessPOList(businessPOList []*po.Business) {
	*bl = make([]*Business, 0, len(businessPOList))
	for _, businessPO := range businessPOList {
		business := new(Business)
		business.fillBusinessFromBusinessPO(businessPO)
		*bl = append(*bl, business)
	}
}

func (b *Business) String() string {
	return fmt.Sprintf("{id=%d, name=%s, desc=%s}", b.ID, b.Name, b.Desc)
}
