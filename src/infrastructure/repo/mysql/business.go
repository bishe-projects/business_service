package mysql

import (
	"gitlab.com/bishe-projects/business_service/src/infrastructure/repo/po"
	"gitlab.com/bishe-projects/common_utils/error_enum"
	"gitlab.com/bishe-projects/common_utils/mysql"
)

var (
	BusinessMySQLDao = new(Business)
)

type Business struct{}

const businessTable = "business"

func (r *Business) CreateBusiness(business *po.Business) error {
	err := db.Table(businessTable).Create(&business).Error
	if err != nil {
		if code := mysql.MySqlErrCode(err); code == mysql.ErrDuplicateEntryCode {
			return error_enum.ErrDuplicateEntry
		}
		return err
	}
	return nil
}

func (r *Business) GetBusinessByID(businessID int64) (*po.Business, error) {
	var business *po.Business
	result := db.Table(businessTable).First(&business, businessID)
	return business, result.Error
}

func (r *Business) GetBusinessByName(businessName string) (*po.Business, error) {
	var business *po.Business
	result := db.Table(businessTable).Where("name = ?", businessName).First(&business)
	return business, result.Error
}

func (r *Business) AllBusinessList() ([]*po.Business, error) {
	var businessList []*po.Business
	result := db.Table(businessTable).Find(&businessList)
	return businessList, result.Error
}
