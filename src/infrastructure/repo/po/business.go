package po

type Business struct {
	ID   int64
	Name string
	Desc string
}
