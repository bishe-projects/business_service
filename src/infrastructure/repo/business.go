package repo

import (
	"gitlab.com/bishe-projects/business_service/src/infrastructure/repo/mysql"
	"gitlab.com/bishe-projects/business_service/src/infrastructure/repo/po"
)

type BusinessDao interface {
	CreateBusiness(business *po.Business) error
	GetBusinessByID(int64) (*po.Business, error)
	GetBusinessByName(string) (*po.Business, error)
	AllBusinessList() ([]*po.Business, error)
}

var BusinessMySQLRepo = NewBusinessRepo(mysql.BusinessMySQLDao)

type Business struct {
	BusinessDao BusinessDao
}

func NewBusinessRepo(businessDao BusinessDao) *Business {
	return &Business{
		BusinessDao: businessDao,
	}
}
