package business_error

import "gitlab.com/bishe-projects/common_utils/business_error"

// internal server error
var (
	GetBusinessByIDErr    = business_error.NewBusinessError("get business by id failed", -10001)
	GetBusinessByNameErr  = business_error.NewBusinessError("get business by name failed", -10002)
	GetAllBusinessListErr = business_error.NewBusinessError("get all business list failed", -10003)
	CreateBusinessErr     = business_error.NewBusinessError("create business failed", -10004)
	BusinessExistsErr     = business_error.NewBusinessError("the business already exists", -10005)
)
