package application

import (
	"gitlab.com/bishe-projects/business_service/src/domain/business/entity"
	"gitlab.com/bishe-projects/business_service/src/domain/business/service"
	biz_error "gitlab.com/bishe-projects/business_service/src/infrastructure/business_error"
	"gitlab.com/bishe-projects/common_utils/business_error"
)

var BusinessApp = new(Business)

type Business struct{}

func (a *Business) CreateBusiness(business *entity.Business) *business_error.BusinessError {
	err := service.BusinessDomain.CreateBusiness(business)
	if err == business_error.DataExistsErr {
		return biz_error.BusinessExistsErr
	}
	return err
}

func (a *Business) GetBusinessByID(businessID int64) (*entity.Business, *business_error.BusinessError) {
	return service.BusinessDomain.GetBusinessByID(businessID)
}

func (a *Business) GetBusinessByName(businessName string) (*entity.Business, *business_error.BusinessError) {
	return service.BusinessDomain.GetBusinessByName(businessName)
}

func (a *Business) AllBusinessList() ([]*entity.Business, *business_error.BusinessError) {
	return service.BusinessDomain.AllBusinessList()
}
