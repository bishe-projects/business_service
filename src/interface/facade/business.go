package facade

import (
	"context"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/business_service/src/application"
	"gitlab.com/bishe-projects/business_service/src/interface/assembler"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/business"
)

var BusinessFacade = new(Business)

type Business struct{}

func (f *Business) CreateBusiness(ctx context.Context, req *business.CreateBusinessReq) *business.CreateBusinessResp {
	resp := &business.CreateBusinessResp{}
	businessEntity := assembler.ConvertCreateBusinessReqToBusinessEntity(req)
	err := application.BusinessApp.CreateBusiness(businessEntity)
	if err != nil {
		klog.CtxErrorf(ctx, "[BusinessFacade] create business failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.Business = assembler.ConvertBusinessEntityToBusiness(businessEntity)
	return resp
}

func (f *Business) GetBusinessByID(ctx context.Context, req *business.GetBusinessByIDReq) *business.GetBusinessByIDResp {
	resp := &business.GetBusinessByIDResp{}
	businessEntity, err := application.BusinessApp.GetBusinessByID(req.BusinessId)
	if err != nil {
		klog.CtxErrorf(ctx, "[BusinessFacade] get business by id failed: req=%+v err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	if businessEntity != nil {
		resp.Business = assembler.ConvertBusinessEntityToBusiness(businessEntity)
	}
	return resp
}

func (f *Business) GetBusinessByName(ctx context.Context, req *business.GetBusinessByNameReq) *business.GetBusinessByNameResp {
	resp := &business.GetBusinessByNameResp{}
	businessEntity, err := application.BusinessApp.GetBusinessByName(req.BusinessName)
	if err != nil {
		klog.CtxErrorf(ctx, "[BusinessFacade] get business by name failed: req=%+v err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	if businessEntity != nil {
		resp.Business = assembler.ConvertBusinessEntityToBusiness(businessEntity)
	}
	return resp
}

func (f *Business) AllBusinessList(ctx context.Context, req *business.AllBusinessListReq) *business.AllBusinessListResp {
	resp := &business.AllBusinessListResp{}
	businessEntityList, err := application.BusinessApp.AllBusinessList()
	if err != nil {
		klog.CtxErrorf(ctx, "[BusinessFacade] get all business failed: req=%+v err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.BusinessList = assembler.ConvertBusinessEntityListToBusinessList(businessEntityList)
	return resp
}
