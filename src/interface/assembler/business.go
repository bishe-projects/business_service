package assembler

import (
	"gitlab.com/bishe-projects/business_service/src/domain/business/entity"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/business"
)

func ConvertCreateBusinessReqToBusinessEntity(req *business.CreateBusinessReq) *entity.Business {
	return &entity.Business{
		Name: req.Name,
		Desc: req.Desc,
	}
}

func ConvertBusinessEntityToBusiness(businessEntity *entity.Business) *business.Business {
	return &business.Business{
		Id:   businessEntity.ID,
		Name: businessEntity.Name,
		Desc: businessEntity.Desc,
	}
}

func ConvertBusinessEntityListToBusinessList(businessEntityList []*entity.Business) []*business.Business {
	businessList := make([]*business.Business, 0, len(businessEntityList))
	for _, businessEntity := range businessEntityList {
		businessList = append(businessList, ConvertBusinessEntityToBusiness(businessEntity))
	}
	return businessList
}
